\documentclass[a4paper,10pt,french,notumble,nofoldmark]{leaflet}

%% Définition des polices %%
\usepackage{fontspec}
\setmainfont{Liberation Serif}
\setsansfont{Liberation Sans}
\setmonofont{Liberation Mono}

% Pour le fonds de la page de garde
\usepackage{wallpaper}

%% Définition des boîtes %%
\usepackage{tcolorbox}
\tcbuselibrary{skins}

%% Définition des couleurs %%
%% on utilise des focntionnalités d'xcolor chargé par tcolorbox
\definecolor{SectionLA}{HTML}{5555A0}
\definecolor{ReliefLA}{HTML}{0000B3}
\definecolor{TourBoite}{HTML}{2C558B}
\definecolor{LeftBoite}{HTML}{FFF7EF}
\definecolor{RightBoite}{HTML}{FFD491}

%% Boîte image %%
\newtcolorbox{boiteimage}{colback=white,boxrule=0.5pt}

%% Boîte flash
\newtcolorbox{flash}{
  boxrule=0.5pt,
  colframe=TourBoite,
  halign=center,
  colback=LeftBoite}

\usepackage{adjustbox}

%% Personnalisation des titres de sectionnement
\usepackage{sectsty}

\sectionfont{\color{SectionLA}\centering}
\subsectionfont{\centering\underline}

\usepackage[normalem]{ulem} % permet un meilleur soulignement dans sectsty

%% Pour avoir des images décalées
\usepackage{wrapfig}

%% Pour la présentation des nombres
\usepackage[np]{numprint}
 
\usepackage{babel}
\frenchbsetup{og=«,fg=»}

\usepackage{caption}
\captionsetup{labelformat=empty,font=scriptsize,justification=centering}

%% Pour les url
\usepackage{hyperref}
\hypersetup{colorlinks,
  urlcolor=ReliefLA}

%% Commandes perso %%
\newcommand{\relief}[1]{\textcolor{ReliefLA}{#1}}


\begin{document}

\ThisULCornerWallPaper{1}{img/Fond}

\begin{center}
  \Huge
  \sffamily
  LOGICIELS

  \textbf{LIBRES}
\end{center}


\begin{adjustbox}{minipage=.5\textwidth,margin=0pt \smallskipamount,center}
  \begin{boiteimage}
    \centering
    \textsf{ASSOCIATION}

    \includegraphics[width=.8\linewidth]{img/LogoLA}
  \end{boiteimage}
\end{adjustbox}

\begin{center}
\sffamily
  \huge UNE AUTRE \\ INFORMATIQUE \\ EST POSSIBLE
\end{center}

\begin{adjustbox}{minipage=\textwidth,margin=0pt \smallskipamount,center}
\centering
\includegraphics[width=.8\linewidth]{img/Spirale}
\end{adjustbox}

\newpage

\section{Que sont les logiciels libres ?}

\begin{flash}
  Tout logiciel libre garantit à l'utilisateur \relief{quatre
    libertés}:
  \begin{itemize}
  \item \relief{utiliser} le logiciel pour quelque usage que se soit;
  \item \relief{analyser} (ou faire analyser) le code source du
    programme (sa recette de fabrication);
  \item \relief{redistribuer} des copies;
  \item \relief{améliorer} (ou faire améliorer) le programme.
  \end{itemize}
\end{flash}

Les logiciels libres regroupent aujourd'hui plusieurs dizaines de milliers de programmes de tous types, développés collectivement à l'échelle mondiale.

Ils constituent un \relief{bien commun}, inscrit d'ailleurs au
patrimoine mondial de l'UNESCO, et libèrent l'utilisateur de son
simple rôle de consommateur.

De nombreuses personnes ne sachant pas programmer participent aux
logiciels libres, par exemple en :
\begin{itemize}
\item traduisant des logiciel et de la documentation;
\item créant des donnéeslibres (logos, images, sons, film…);
\item signalant des problèmes aux programmeurs;
\item aisant des dons à des fondations (Wikimedia, Mozilla, Free
  Software Fondation…);
\item faisant la promotion du logiciel libre;
\item aidant les autres à utiliserdes logiciels libres.
\end{itemize}

Ils permettent à chacun d'utiliser une informatique \relief{pérenne, fiable,
sécurisée et à moindre coût}.

Ils font primer le \relief{partage des connaissances} quand d'autres font primer les bénéfices de quelques actionnaires.

\newpage

\section{Que peut apporter un logiciel libre ?}

\subsection{Une meilleure sécurité et fiabilité}
Les logiciels libres sont conçus afin d'offrir les meilleures garanties en termes de :
\begin{itemize}
  \item \relief{sécurité} (\relief{moins sensibles aux virus}, réactivité pour corriger les failles…);
  \item \relief{respect de la vie privée} (les logiciels non-libres sont des boîtes noires: que font-ils avec vos données?).
\end{itemize}

\subsection{La gratuité en tout légalité}
Tous les libristes vous le diront, « libre » ne veut pas juste dire « gratuit ». C'est bien plus que cela !

Dans le monde du logiciel libre il n'y a \relief{pas de piratage}, mais \relief{uniquement du partage}.

Par exemple, des élèves peuvent repartir avec une clé USB contenant tous les logiciels libres utilisés en classe, pour \relief{les installer légalement chez eux}.
\begin{flash}
  Les logiciels libres les plus populaires (Firefox, LibreOffice, VLC…) sont disponibles sous Windows, MacOS, Linux… pas de discrimination.
\end{flash}

\subsection{La pérennité de vos documents}
Les logiciels libres utilisent des formats \relief{standards} et \relief{ouverts} pour enregistrer leurs données (textes, images…).

Ainsi vos fichiers seront \relief{toujours lisibles dans de nombreuses années}.

Cela favorise aussi l'\relief{inter-opérabilité}, permettant de lire et modifier facilement les fichiers échangés avec vos collègues et amis, quel que soit l'âge ou le type d'ordinateur qu'ils utilisent.

\section{Qu'est-ce que GNU/Linux ?}
De nombreux utilisateurs pensent que pour faire fonctionner un
ordinateur, il n'y a le choix qu'entre \relief{Windows} et \relief{MacOS}.

D'autres systèmes (pour la plupart libres) existent. Linux est le plus
utilisé.

\begin{wrapfigure}{r}{.16\linewidth}
  \centering
  \includegraphics[width=.8\linewidth]{img/Tux}
  \caption{Tux la mascotte de Linux}
\end{wrapfigure}
\relief{Linux} est développé via Internet par des milliers
d'informaticiens, il est souvent combiné avec les logiciels issus du
projet GNU (le projet fondateur de l'idéologie du logiciel libre).

Linux associé à une sélection de logiciels libres pré-installés se
nomme une distribution. Parmi celles conseillées aux débutants, on
peut citer \relief{Ubuntu} et \relief{Linux Mint}.

\begin{flash}
Utiliser Linux quand on est habitué à un autre système, c'est comme
utiliser un deux-roues quand on est accoutumé à la voiture : c'est
différent et il faut mettre de côté une partie de ses habitudes.

\relief{Linux Arverne peut vous aider} pour sa découverte et son installation.
\end{flash}

\subsection{Vous utiliser déjà Linux sans le savoir}

\begin{itemize}
\item La plupart des « box » (\relief{Livebox}, \relief{Freebox}…)
  sont basées sur Linux.
\item Beaucoup de serveurs informatiques, dont ceux de \relief{Google},
  l'utilisent.
\item \relief{Android}, qui équipe la majorité des \relief{smartphones}, a pour base Linux (sans les logiciels GNU).
\end{itemize}

\begin{flash}
Il faut signaler qu'il est possible d'installer Linux en cohabitation avec Windows ou MacOS, avec choix au démarrage de l'ordinateur.
\end{flash}

\newpage

\section{Aller plus loin}

\subsection{D'autres acteurs du logiciel libre en France}

\begin{center}
  \url{www.framasoft.net}
\end{center}
C'est le site francophone de référence pour découvrir la diversité des
logiciels libres.

Framasoft propose, dans sa section Framakey, des logiciels libres «
nomades » : \relief{ils s'installent sur des clés USB}, et s'utilisent sans modifier l'ordinateur emprunté.

Framasoft propose aussi des services collaboratifs équivalents à ceux
proposés entre autres par Google, mais libres et respectueux de la vie privée.

\begin{center}
  \url{www.april.org}
\end{center}
Pionnière du logiciel libre en France, l'April est constituée de plus de \np{4000}~adhérents (individus, entreprises, associations et organisations). C'est un acteur majeur de la démocratisation et de la diffusion du logiciel libre et des standards ouverts auprès du grand public, des professionnels et des institutions dans l'espace francophone.

\subsection{Ils utilisent des logiciels libres}
\begin{itemize}
\item De nombreuses administrations (Gendarmerie nationale, Direction
  générale des impôts…).
\item De nombreuses municipalités : Munich, Turin, Toulouse…
\item Le CEA, la NASA (dans la Station Spatiale Internationale, pour
  piloter les robots sur Mars, etc.).
\item PSA (Peugeot/Citroën).
\item Le Monde Diplomatique.
\item Google, IBM.
\item DELL, Hewlett-Packard, Samsung.
\item La bourse de Londres.
\end{itemize}

\section{L'association Linux Arverne}

\subsection{Qui sommes nous ?}
Linux Arverne est une \relief{association} loi de 1901 qui vise à
promouvoir le Libre en Auvergne.

Elle propose pour cela différentes activités dont :
\begin{itemize}
\item \relief{aide à l'installation} de logiciels libres ;
\item \relief{découverte} de leurs fonctionnalités…
\end{itemize}

\subsection{Comment nous contacter ?}

\begin{itemize}
\item Par mail :
  \href{mailto:contact@linuxarverne.org}{contact@linuxarverne.org}.
\item Sur notre site web : \url{www.linuxarverne.org}.
\item Par courrier:
\end{itemize}
\hfill
\begin{minipage}{.7\linewidth}
  Linux Arverne -- Maison des associations

  21 rue Jean Richepin

  63000~Clermont-Ferrand. 
\end{minipage}

\subsection{Nous rencontrer}
Notre local est situé dans le centre Jean Richepin, qui est en aval de la rue Montlosier.

Des \relief{permanences} ont lieu les \relief{jeudis soirs} de 20h30 à
22h30.

Des \relief{ateliers thématiques mensuels} sont annoncés sur notre
site web.

Adressez-vous au gardien ou consultez notre site web pour situer notre
local dans le centre.


\begin{adjustbox}{minipage=.93\textwidth,margin=0pt \smallskipamount,center}
  \centering
  \includegraphics[width=\linewidth]{img/PlanLA}
\end{adjustbox}

\marginpar{%
  \rotatebox{90}{%
    \begin{minipage}{\textheight}
      \scriptsize
      IPNS. Ne pas jeter sur la voie publique – Oct.~2014

      CC-by-sa: Linux Arverne, Wikipédia, J-C~Becquet, Oisux
    \end{minipage}
  }
}

\end{document}

% Local Variables:
% TeX-engine: luatex % nécessité par fontspec
% coding: utf-8-unix
% TeX-master: t
% End:
